module Turtle where
import System.IO 
import System.Random
import Control.Monad

data Point = Point Float Float deriving (Show) 
data Size = Size Int Int deriving (Show) 
data Shape = Rect Point Size String| Circle Point Float String | Path String String | Line Point Point String | Turtle Point Point deriving (Show)
data Ecran = Ecran Size [Shape] deriving (Show) 
data Up = Up Bool deriving (Show) 
data Tortue = Tortue Point Up Float deriving (Show) -- tortue position crayonState(baissé|levé) angle
data Monde = Monde Tortue Ecran deriving (Show) 
data Command = Avancer Float | Reculer Float | Gauche Float | Droite Float  | Lever | Baisser | Position Point deriving (Show)   
-- recupérer les shapes et les prints


-- Path [Point]

-- shapeToString:: Shape -> String 
shapeToString :: Shape -> [Char]
shapeToString (Rect (Point x y) (Size w h) col) = "<rect x='"++(show x)++"' y='"++(show y)++"' width='"++(show w)++"' height='"++(show h)++"' stroke='"++col++"' fill='transparent' stroke-width='5'/> \n"
shapeToString (Circle (Point x y) r col) = "<circle cx='"++ (show x)++"' cy='"++(show y)++"' r='"++(show r)++"' stroke='"++col++"' fill='transparent' stroke-width='5'/>\n"
shapeToString (Path s col) = "<path d='"++s++"' fill='none' stroke='"++col++"' stroke-width='5'/>"
shapeToString (Line (Point x1 y1) (Point x2 y2) col) = "<line x1='"++(show x1)++"' x2='"++(show x2)++"' y1='"++(show y1)++"' y2='"++(show y2)++"' stroke='"++col++"' fill='transparent' stroke-width='5'/>\n"
shapeToString (Turtle (Point x1 y1) (Point x2 y2)) = "<line x1='"++(show x1)++"' x2='"++(show x2)++"' y1='"++(show y1)++"' y2='"++(show y2)++"' stroke='black' fill='transparent' stroke-width='1'/>\n"

shapesToString :: [Shape] -> [Char]
shapesToString [] = ""
shapesToString (h:t) = shapeToString h ++ shapesToString t

export (Ecran (Size w h) shapes) = "<svg width=\""++(show w)++"\" height=\""++(show h)++"\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">\n"++(shapesToString shapes)++"</svg>\n" 

-- Dessiner des carré aleatoire sur l'ecran
minSize = 50
maxSize = 200
minX = 0
maxX = 300
minY = 0
maxY = 300
colors = ["blue","green","red","orange","yellow","purple","darkgoldenrod","deeppink","lightskyblue","orangered","navy","yellow"]

randomElemList l = do
  i <- randomRIO(0, length l - 1)
  return $ (l !! i) 


randomValueRGB :: IO Int
randomValueRGB = do
  i <- randomRIO(0, 255)
  return (i)

-- random RGB Color
randomColor :: IO [Char]
randomColor = do
  r <- randomValueRGB
  g <- randomValueRGB
  b <- randomValueRGB
  return $ "rgb("++(show r)++","++(show g)++","++(show b)++")"  


-- 1.2 Quelques dessins

randomSquare :: IO Shape
randomSquare =  do
    size <- randomRIO (minSize, maxSize)
    x <- randomRIO (minX, maxX)
    y <- randomRIO (minY, maxY)
    color <- randomColor
    return (Rect (Point x y) (Size size size) color)


-- TURTLES


abs :: Integer -> Integer  -- « Integer » veut dire « nombre entier »
abs x
 | x >= 0 = x
 | otherwise = -x

toInt :: Float -> Int
toInt x = round x

degToRad :: Floating a => a -> a
degToRad deg = deg * pi / 180

right :: Monde -> Float -> Monde
right (Monde (Tortue p c a) ecran) an = (Monde (Tortue p c (realToFrac angle)) ecran) 
  where angle = mod (toInt (a + an)) 360 

left :: Monde -> Float -> Monde
left (Monde (Tortue p c a) ecran) an= (Monde (Tortue p c (realToFrac angle)) ecran) 
  where angle = mod (toInt (a - an)) 360 

up :: Monde -> Monde
up (Monde (Tortue p (Up _) a) ecran) = (Monde (Tortue p (Up False) a) ecran)

down :: Monde -> Monde
down (Monde (Tortue p (Up _) a) ecran)= (Monde (Tortue p (Up True) a) ecran)

forward :: Monde -> Float -> Monde
forward (Monde (Tortue (Point x y) (Up c) a) (Ecran (Size h w) shapes)) l 
    | c = (Monde (Tortue (Point newX newY) (Up c) a) (Ecran (Size h w) (shapes ++ [Turtle (Point x y) (Point newX newY)])))
    | otherwise = (Monde (Tortue (Point newX newY) (Up c) a) (Ecran (Size h w) shapes))
    where
       newX = x + (cos (degToRad a)) * l
       newY = y + (sin (degToRad a)) * l 

setPosition :: Monde -> Point -> Monde
setPosition (Monde (Tortue p c a) ecran) newP= (Monde (Tortue newP c a) ecran)

executer_ordre :: Monde -> Command -> Monde
executer_ordre monMonde (Avancer x) = forward monMonde x
executer_ordre monMonde (Reculer x) = forward monMonde (-x)
executer_ordre monMonde (Baisser) = down monMonde
executer_ordre monMonde (Lever) = up monMonde
executer_ordre monMonde (Gauche a) = left monMonde a
executer_ordre monMonde (Droite a) = right monMonde a
executer_ordre monMonde (Position p) = setPosition monMonde p

executer_programme :: Monde -> [Command] -> Monde
executer_programme mondeFinal [] = mondeFinal
executer_programme mondeInitial (command:t) =  executer_programme (executer_ordre mondeInitial command) t



exportTurtle :: Monde -> [Char]
exportTurtle  (Monde _ (Ecran (Size w h) shapes)) = "<svg width=\""++(show w)++"\" height=\""++(show h)++"\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">\n"++(shapesToString shapes)++"</svg>\n"

-- Fonction repeter


repeter :: Float -> [Command] -> [Command]
repeter 0 c = []
repeter x shape = 
    shape ++ (repeter (x-1) shape)


-- Définir une fonction pour dessiner le flocon de Von Kock
von_koch :: (Eq a, Num a) => a -> Float -> [Command]
von_koch 0 l = [Avancer l]
von_koch n l 
    = von_koch (n-1) l ++ [Gauche 60] ++ von_koch (n-1) l ++ [Droite 120] ++ von_koch (n-1) l ++ [Gauche 60] ++ von_koch (n-1) l


flocon_von_koch n l = (repeter 2 (von_koch n l ++ [Gauche 240])) ++ von_koch n l
-- Main 


