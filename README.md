# Projet Programmation Fonctionelle (Haskell)

## Modelisation de la tortue et du monde

Le but du projet était de pouvoir mettre en place **une tortue** de type logo, qui soit capable d'exécuter des ordres  qui lui seront donné grâce à son crayon capable de laisser des traces ou non lors de son déplacement.


Etant donné que le langage Haskell ne possède pas sortie graphique qui permette de dessiner  simples les formes, nous nous sommes servis du navigateur web en définissant de formes géométriques grâce à la balise svg au format xlm.

Comme demandé, j'ai commencé par créer une image Haskell puis je l'ai exporté en svg grace à la fonction export.
```haskell
export (Ecran (Size w h) shapes) = "<svg width=\""++(show w)++"\" height=\""++(show h)++"\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">\n"++(shapesToString shapes)++"</svg>\n"
```

J'ai défini différents Types de représentation de données afin de pouvoir gérer au mieux l'environnement de la tortue dans son monde.

```haskell
data Point = Point Float Float deriving (Show)
data Size = Size Int Int deriving (Show)
data Shape = Rect Point Size String| Circle Point Float String | Path String String | Line Point Point String | Turtle Point Point deriving (Show)
data Ecran = Ecran Size [Shape] deriving (Show)
data Up = Up Bool deriving (Show)
data Tortue = Tortue Point Up Float deriving (Show)
data Monde = Monde Tortue Ecran deriving (Show)
data ColorHSLA = ColorHSLA Int Int Int Int deriving (Show)
data Commands = Avancer Float | Gauche Float | Droite Float  | Lever | Baisser deriving (Show)  
```

## Lancer le programme

Mon projet contient 2 fichiers et 1 dossiers :

|  nom |  descriptions |
| ------------ | ------------ |
|  turtle.hs |  fichier du module de la tortue  |
|  main.hs |  fichier du module principale   |
|  shapes |  dossier qui contiendra tous les fichiers de sortie du programme |

**Pour pouvoir lancer le programme** :

1. Telecharger le projet (Décompresser si nécessaire)

2. Ouvrez un terminal, et aller dans le dossier du projet.
`$ cd ../projet-mubuanga` : Veillez à mettre le bon répertoire du projet

3. Lancer Haskell:
`$ ghci` : Pour ma part c'est avec la commande ghci

4. Compiler le projet (le fichier principale):
`$ :load main`

5. Ci-dessous la liste des commandes à executer pour  :

- Dessine  4 figures géométriques.
`$ mainCanva`
  - le fichier de sortie  : `shapes/shapes.html`
 
- Dessine  n carrés  de couleur et de taille et de couleur aléatoires.
`$ mainRandomSquare n`
ex  usage :  `$ mainRandomSquare 50`
 - le fichier de sortie  : `shapes/randomSquares.html`
 
- Dessine un carré, un polygone et un cercle approximatif avec une tortue
`$ mainMoulinTurtle`
  - le fichier de sortie  : `shapes/shapesTurtles.html`
 
- Dessine un moulin
`$ mainMoulinTurtle`
  - le fichier de sortie  : `shapes/moulin.html`
 
 
  - Dessine un flocon de Von Koch de dimension 2
`$ mainFloconVonKoch n l`
ex  usage :  `$ mainFloconVonKoch 1 50`
  - le fichier de sortie  : `shapes/flocon_von_koch.html`

- Une fonction qui applique toutes les fonctions ci-hauts
`$ main`
 
