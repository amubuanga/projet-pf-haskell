import Turtle
import System.IO 
import System.Random
import Control.Monad
-- FUNCTIONS MAINS 

mainCanva = let 
   rectSize = Size 30 30 
   screenSize = Size 200 250  
   p1 = Point 10 50 
   p2 = Point 110 150
   c = Point 25 75
   d = "M20,230 Q40,205 50,230 T90,230"  
   rect = Rect p1 rectSize "blue"
   path = Path d "red"
   circle = Circle c 20 "red"
   line = Line p1 p2 "orange"
   screen = Ecran screenSize [rect, path, circle, line]
   svg = export screen
   in do 
       -- print $ svg
       f <- openFile "shapes/shapes.html" WriteMode
       hPutStrLn f "<?xml version=\"1.0\" standalone=\"no\"?>\n"
       hPutStrLn f svg
       hClose f 


mainRandomSquare n = do
  squares <- replicateM n randomSquare
  do 
       -- print $ svg
       f <- openFile "shapes/randomSquares.html" WriteMode
       hPutStrLn f "<?xml version=\"1.0\" standalone=\"no\"?>\n"
       hPutStrLn f (export ((Ecran (Size 500 500)) squares))
       hClose f 

{-  **************** MAIN TURTLES **************** -}

screenSize = Size 1000 1000
tortue = Tortue (Point 500 500) (Up True) 0
screen = Ecran screenSize []
p2 = Point 350 500 
p3 = Point 700 500 

-- Définir une fonction qui permet de faire dessiner un carré à la tortue.
-- Définir une fonction qui permet de faire dessiner un polygone régulier à la tortue. Dessiner ainsi
-- une approximation de cercle.


mainShapesTurtle = let 
  -- commands = [Avancer 100,Gauche 90,Avancer 100,Gauche 90,Avancer 100,Gauche 90,Avancer 100,Gauche 90]
  commandsSquare = repeter 4 [Avancer 100,Gauche 90]
  commandsCircle = (Position p2):repeter 20 [Avancer 20,Gauche 18.947]
  commandsPolygon = (Position p3):repeter 5 [Avancer 100,Gauche 72]
  commands = commandsSquare ++ commandsPolygon ++ commandsCircle
  monMonde = Monde tortue screen
  newMonde  = executer_programme monMonde commands
  svg = exportTurtle newMonde
  in do 
       -- print $ svg
       f <- openFile "shapes/shapesTurtle.html" WriteMode
       hPutStrLn f "<?xml version=\"1.0\" standalone=\"no\"?>\n"
       hPutStrLn f svg
       hClose f  

--  Définir une fonction qui permet de faire dessiner un moulin comme ceci :

mainMoulinTurtle = let 
  l = 100
  commandsSquare = repeter 4 [Avancer l,Gauche 90]

  commandsDroit = [Gauche 45, Avancer l]++commandsSquare ++ [Gauche 180,Avancer l] ++ [Lever ,Avancer l, Baisser]++commandsSquare ++ [Gauche 180,Avancer l]
  tourner = [Gauche 45]
  commandsGauche = [Gauche 45, Avancer l]++commandsSquare ++ [Gauche 180,Avancer l] ++ [Lever ,Avancer l, Baisser]++commandsSquare ++ [Gauche 180,Avancer l]
  commands = commandsDroit ++ tourner ++ commandsGauche
  monMonde = Monde tortue screen
  newMonde  = executer_programme monMonde commands
  svg = exportTurtle newMonde
  in do 
       -- print $ svg
       f <- openFile "shapes/moulin.html" WriteMode
       hPutStrLn f "<?xml version=\"1.0\" standalone=\"no\"?>\n"
       hPutStrLn f svg
       hClose f  


-- n dimension 
-- l longueur 
mainVonKoch n l= let 
  screenSize = Size 3000 3000
  tortue = Tortue (Point 200 600) (Up True) 0
  screen = Ecran screenSize []
  commands = von_koch n l
  monMonde = Monde tortue screen
  newMonde  = executer_programme monMonde commands
  svg = exportTurtle newMonde
  in do 
       f <- openFile "shapes/von_koch.html" WriteMode
       hPutStrLn f "<?xml version=\"1.0\" standalone=\"no\"?>\n"
       hPutStrLn f svg
       hClose f  

-- n dimension 
-- l longueur 
mainFloconVonKoch n l= let 
  screenSize = Size 3000 3000
  tortue = Tortue (Point 200 600) (Up True) 0
  screen = Ecran screenSize []
  commands = flocon_von_koch n l
  monMonde = Monde tortue screen
  newMonde  = executer_programme monMonde commands
  svg = exportTurtle newMonde
  in do 
       f <- openFile "shapes/flocon_von_koch.html" WriteMode
       hPutStrLn f "<?xml version=\"1.0\" standalone=\"no\"?>\n"
       hPutStrLn f svg
       hClose f  


main = do
  mainFloconVonKoch 2 20
  mainVonKoch 4 10
  mainMoulinTurtle 
  mainShapesTurtle
  mainCanva
  mainRandomSquare 100
